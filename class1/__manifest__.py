# -*- coding: utf-8 -*-
{
    'name': "class1",

    'summary': """
        To extend default module for modifying purposes.""",

    'description': """
        
    """,

    'author': "diwap",
    'website': "diwap.com.np",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'application',
    'version': '1.0',
    'application': True,

    # any module necessary for this one to work correctly
    'depends': ['base','project'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/views.xml',
    ],
}

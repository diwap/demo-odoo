# -*- coding: utf-8 -*-

from odoo import models, fields

class class1(models.Model):
    _inherit = 'project.task'

    test = fields.Char("Test Field")

class class2(models.Model):
    _name = 'test.address'

    name = fields.Char("Name")
    address = fields.Char("Address")
    number = fields.Char("Number")

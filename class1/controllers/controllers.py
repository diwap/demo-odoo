# -*- coding: utf-8 -*-
from odoo import http

# class Class1(http.Controller):
#     @http.route('/class1/class1/', auth='public')
#     def index(self, **kw):
#         return "Hello, world"

#     @http.route('/class1/class1/objects/', auth='public')
#     def list(self, **kw):
#         return http.request.render('class1.listing', {
#             'root': '/class1/class1',
#             'objects': http.request.env['class1.class1'].search([]),
#         })

#     @http.route('/class1/class1/objects/<model("class1.class1"):obj>/', auth='public')
#     def object(self, obj, **kw):
#         return http.request.render('class1.object', {
#             'object': obj
#         })